#!/usr/bin/bash
chmod +x

if [ ! -e .gitignore ]
then
echo -e 'Downloading Swift .gitignore...'
curl -o .gitignore https://www.gitignore.io/api/swift
fi

echo -e 'Installing xcpretty...'
sudo gem install xcpretty

if [ ! -e /usr/local/bin/gitlab-runner ] 
then
echo -e 'Installing Gitlab-Runner...'
sudo curl --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-darwin-amd64
fi

echo -e 'Register project with Gitlab-Runner...'

sudo gitlab-runner register

gitlab-runner install
gitlab-runner start

curl --output .gitlab-ci.yml https://gitlab.com/ryan.inman/ios-infra/raw/master/ci/.gitlab-ci.yml
open .gitlab-ci.yml
